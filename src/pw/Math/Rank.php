<?php
namespace pw\Math;

class Rank implements \ArrayAccess, \Iterator, \Countable {
    /**
     * The rank:
     * ['A' => 1, 'B' => 2, 'C' => 2, 'D' => 4]
     * @var array
     */
    protected $rank;
    
    /**
     * The sorting direction 
     * @var string
     */
    protected $direct;
	
	protected $nties;
	
	const TIE_IGNORE  = 0;
	const TIE_KEEP    = 1;
	const TIE_AVERAGE = 2;
    
    /**
     * Construct a rank use an array like:
     * [ 'A' => val1, 'B' => val2, ... ]
     * Entries are sorted in descending order of val1, val2, ... by default 
     * 
     * @param array $array The array
     * @param string $direct The sorting direction (desc|asc)
     * @param string $tie Method to rank the tied elements
     * 	TIE_IGNORE:  Normally rank, rank index increased as normal
     * 	TIE_KEEP:    Rank index kept as the first tied element
     * 	TIE_AVERAGE: Take the average for all tied elements
     */
    public function __construct($array, $direct = 'desc', $tie = self::TIE_KEEP) {
        $this->rank  = $array; // keep key sorting
		$this->nties = array();
        $direct == 'asc'
            ? asort ($array)
            : arsort($array);
        $this->direct = $direct == 'asc' ? 'asc' : 'desc';
        
		if ($tie == self::TIE_AVERAGE) {
			$r = 1;
			$lastvalue = null;
			$tied = array();
			foreach ($array as $key => $value) {
				if (is_null($lastvalue) or $lastvalue!=$value) {
					$lastvalue = $value;
					if (!empty($tied)) {
						$avg = $this->rank[$tied[0]] + (sizeof($tied) - 1) / 2;
						$this->nties[] = sizeof($tied);
						foreach ($tied as $tie)
							$this->rank[$tie] = $avg;
						$tied = [];
					}
				} else {
					$tied[] = $key;
				}
				$this->rank[$key] = $r++;
			}
			if (!empty($tied)) {
				$avg = $this->rank[$tied[0]] + (sizeof($tied) - 1) / 2;
				$this->nties[] = sizeof($tied);
				foreach ($tied as $tie)
					$this->rank[$tie] = $avg;
			}
		} elseif ($tie == self::TIE_IGNORE) {
			$r = 1;
			foreach ($array as $key => $value) {
				$this->rank[$key] = $r++;
			}
		} else {
			$i = $r = 1;
			$lastvalue = null;
			foreach ($array as $key => $value) {
				if (is_null($lastvalue) or $lastvalue!=$value) {
					$lastvalue = $value;
					if ($i - $r > 1) $this->nties[] = $i - $r;
					$r = $i;
				} else {
					
				}
				$this->rank[$key] = $r;
				$i ++;
			}
			if ($i - $r > 1) $this->nties[] = $i - $r;
		}
    }
    
    /**
     * Reverse the rank
     */
    public function reverse () {
        $r = $this->rank;
        $size = sizeof ($r);
        foreach ($this->rank as $key => $val) {
            $this->rank [$key] = $size + 1 - $val;
        }
		$this->direct = $this->direct == "asc" ? "desc" : "asc";
    }
    
    /**
     * Return the direction
     * @return string  The direction (asc|desc)
     */
    public function direct() {
        return $this->direct;
    }
	
	public function nties () {
		return $this->nties;
	}
    
	/**
	 * Implement get function of array []
	 * @param mixed $offset The index
	 * @return mixed  the value
	 */
    public function offsetGet($offset) {
        return isset($this->rank[$offset]) ? $this->rank[$offset] : null;
    }
	
	public function offsetSet ($offset, $value) {
		if (is_null($offset)) $this->rank[] = $value;
		else $this->rank[$offset] = $value;
	}
	
	public function offsetExists ($offset) {
		return isset($this->rank[$offset]);
	}
	
	public function offsetUnset($offset) {
        unset($this->rank[$offset]);
    }
	
	public function rewind() {
		return reset($this->rank);
	}
	public function current() {
		return current($this->rank);
	}
	public function key() {
		return key($this->rank);
	}
	public function next() {
		return next($this->rank);
	}
	public function valid() {
		return key($this->rank) !== null;
	}
	public function count () {
		return sizeof($this->rank);
	}
	
	public function keys () {
    return array_keys($this->rank);
	}
	
	public function rank () {
    return $this->rank;
	}
	
	public function __toString () {
    $ret = "";
    foreach ($this->rank as $key => $val)
      $ret .= "$key\t$val\n";
    return $ret;
	}
}