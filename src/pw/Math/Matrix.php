<?php
namespace pw\Math;

class Matrix implements \ArrayAccess, \Iterator, \Countable {
	
	protected $data;
	
	public function __construct ($array = array()) {
		if (!empty($array)) {
			if (is_array($array[0])) {
				$size = sizeof($array[0]);
				foreach ($array as $a) {
					if ($size != sizeof($a))
						throw new \InvalidArgumentException('The dimensions of rows are not consistent.');
				}
			} else {
				throw new \InvalidArgumentException('Matrix must be constructed with a 2D array.');
			}
		}
		
		$this->data = $array;
	}
	
	public function fill ($val, $row, $col) {
		$this->data = array_fill(
			0,
			$row,
			array_fill(0, $col, $val)
		);
	}
	
	/**
	 * Fill the matrix with random values
	 * To reproduce, use srand first
	 * 
	 * @param int $row The number of rows
	 * @param int $col The number of columns
	 * @param Number $min The minimum value
	 * @param Number $max The maximum value
	 */
	public function rand_fill ($row, $col, $min = 0, $max = 1) {
		$maxrand = getrandmax();
		for ($i=0; $i<$row; $i++) {
			$this->data[$i] = array();
			for ($j=0; $j<$col; $j++) {
				$this->data[$i][$j] = rand(0, $maxrand) / $maxrand * ($max - $min) + $min;
			}
		}
	}
	
	public function count () {
		$ret = array();
		$ret[0] = sizeof($this->data);
		$ret[1] = isset($this->data[0]) ? sizeof($this->data[0]) : 0;
		return $ret;
	}
	
	/**
	 * Implement get function of array []
	 * @param mixed $offset The index
	 * @example
	 * 	$m = new Matrix([[1,2,3], [4,5,6], [7,8,9]]);
	 * 		 -----------
	 * 		  1 | 2 | 3
	 * 		 -----------
	 * 		  4 | 5 | 6
	 * 		 -----------
	 * 		  7 | 8 | 9
	 * 		 -----------
	 * $m[1]: 
	 * 		 -----------
	 * 		  4 | 5 | 6
	 * 		 -----------
	 * $m[',1']: 
	 * 		 ---
	 * 		  2 
	 * 		 ---
	 * 		  5 
	 * 		 ---
	 * 		  8 
	 * 		 ---
	 * $m['1,1']:
	 * 		 ---
	 * 		  5
	 * 		 ---
	 * $m[':1,:1']: 
	 * 		 -------
	 * 		  1 | 2 
	 * 		 -------
	 * 		  4 | 5 
	 * 		 -------
	 * $m['0:1,:1']: 
	 * 		 -------
	 * 		  1 | 2 
	 * 		 -------
	 * 		  4 | 5 
	 * 		 -------
	 * $m['0:1,:']: 
	 * 		 ----------
	 * 		  1 | 2 | 3
	 * 		 ----------
	 * 		  4 | 5 | 6
	 * 		 ----------
	 * @return mixed  the value
	 */
    public function offsetGet($offset) {
		$ret = array();
		$offsets = array();
		$flag =  $this->_offsetChecker($offset, $offsets);
		if (array_product($flag) == 0)
			throw new \InvalidArgumentException('Offset out of range.');
		
		list ($rowstart, $rowend, $colstart, $colend) = $offsets;
		for ($row = $rowstart; $row <= $rowend; $row++) {
			$ret[$row - $rowstart] = array();
			for ($col = $colstart; $col <= $colend; $col++) {
				$ret[$row - $rowstart][$col - $colstart] = $this->data[$row][$col];
			}
		}
		
		return new self($ret);
    }
	
	public function get($row=0, $col=0) {
		return $this["$row,$col"]->data[0][0];
	}
	
	public function offsetSet ($offset, $value) {
		list ($nrow, $ncol) = $this->count();
		$offsets = array();
		if (is_null($offset)) $offset = $nrow;
		$this->_offsetChecker($offset, $offsets);
		list ($rowstart, $rowend, $colstart, $colend) = $offsets;
		
		$vm = null;
		if (!is_array($value)) {
			$vm = new self();
			$vm->fill ($value, $rowend - $rowstart + 1, $colend - $colstart + 1);
		} else {
			if (!($value instanceof Matrix))
				$vm = new self($value);
			else
				$vm = $value;
		}
		
		list ($vrow, $vcol) = $vm->count();
		if ($vrow != $rowend - $rowstart + 1 or $vcol != $colend - $colstart + 1)
			throw new \InvalidArgumentException('Inconsistent dimension.');
		
		$mrowend = max($rowend, $nrow-1);
		$mcolend = max($colend, $ncol-1);
		for ($i=0; $i<=$mrowend; $i++) {
			for ($j=0; $j<=$mcolend; $j++) {
				if ($i >= $rowstart and $i <= $rowend and $j >= $colstart and $j <= $colend) 
					$this->data[$i][$j] = $vm->get($i-$rowstart, $j-$colstart);
				elseif ($i >= $nrow or $j >= $ncol)
					$this->data[$i][$j] = 0;
			}
		}
	}
	
	private function _offsetChecker($offset, &$offsets = array()) {
		list($nrow, $ncol) = $this->count();
		$rowstart = $colstart = 0;
		$rowend = $nrow - 1;
		$colend = $ncol - 1;
		$ret = array(1, 1, 1, 1); // in range
		
		if (strpos($offset, ',')!==false) {
			list ($p1, $p2) = preg_split("/\s*,\s*/", $offset);
			if (!preg_match("/[:\d]*/", $p1) or !preg_match("/[:\d]*/", $p2))
				throw new \InvalidArgumentException('Invalid offset format.');
			
			if (strpos($p1, ':') !== false) {
				list ($s, $e) = explode(':', $p1);
				if (!empty($s))
					$rowstart = intval ($s);
					
				if ($rowstart >= $nrow) 
					$ret[0] = 0;
				
				if (!empty($e))
					$rowend = intval ($e);
					
				if ($rowend >= $nrow) 
					$ret[1] = 0;
					
				if ($rowend < $rowstart)
					throw new \InvalidArgumentException('Row end is less than row start.');
			} elseif (empty($p1)) {
				
			} elseif (preg_match("/^\d+$/", $p1)) {
				$rowstart = $rowend = intval($p1);
				if ($rowstart >= $nrow)
					$ret[0] = $ret[1] = 0;
			} else {
				throw new \InvalidArgumentException('Unrecognized row index.');
			}
			
			if (strpos($p2, ':') !== false) {
				list ($s, $e) = explode(':', $p2);
				if (!empty($s))
					$colstart = intval ($s);
					
				if ($colstart >= $ncol) 
					$ret[2] = 0;
				
				if (!empty($e))
					$colend = intval ($e);
					
				if ($colend >= $ncol) 
					$ret[3] = 0;
					
				if ($colend < $colstart)
					throw new \InvalidArgumentException('Column end is less than column start.');
			} elseif (empty($p2)) {
				
			} elseif (preg_match("/^\d+$/", $p2)) {
				$colstart = $colend = intval($p2);
				if ($colstart >= $ncol)
					$ret[2] = $ret[3] = 0;
			} else {
				throw new \InvalidArgumentException('Unrecognized column index.');
			}
		} elseif (preg_match ("/^\d+$/", $offset)) {
				$rowstart = $rowend = intval($offset);
				if ($rowstart >= $nrow) 
					$ret[0] = $ret[1] = false;
		} else {
			throw new \InvalidArgumentException('Unrecognized offset.');
		}
		$offsets = array($rowstart, $rowend, $colstart, $colend);
		return $ret;
	}
	
	public function offsetExists ($offset) {
		return array_product($this->_offsetChecker($offset));
	}
	
	public function offsetUnset($offset) {
		$offsets = array();
		$flag =  $this->_offsetChecker($offset, $offsets);
		if (array_product($flag) == 0)
			throw new \InvalidArgumentException('Offset out of range.');
		
		list ($rowstart, $rowend, $colstart, $colend) = $offsets;
		for ($row = $rowstart; $row <= $rowend; $row++) {
			for ($col = $colstart; $col < $colend; $col++) {
				$this->data[$row][$col] = 0;
			}
		}
    }
	
	function rewind() {
		return reset($this->data);
	}
	function current() {
		return current($this->data);
	}
	function key() {
		return key($this->data);
	}
	function next() {
		return next($this->data);
	}
	function valid() {
		return key($this->data) !== null;
	}
	
	public function __toString() {
		$ret = "";
		
		$min = null;
		$max = null;
		
		foreach ($this as $i => $row) {
			$themin = min (array_map("abs", $row));
			$themax = max (array_map("abs", $row));
			if (is_null($min) or $themin < $min)
				$min = $themin;
			if (is_null($max) or $themax > $max)
				$max = $themax;
		}
		if ($max < .1 or $min >= 1e4)
			$format = "%+1.5e"; // +1.11111e+10
		else
			$format = "%+1.4f"; // -11.0000
		$minstr = sprintf($format, $min);
		$maxstr = sprintf($format, $max);
		$maxlen = max (strlen($minstr), strlen($maxstr));
		
		if ($max < .1 or $min >= 1e4) {
			foreach ($this as $i => $row) {
				foreach ($row as $j => $d) {
					$dstr = sprintf($format, $d);
					$len  = $maxlen - strlen($dstr);
					$ret .= substr($dstr, 0, 10) . str_pad(substr($dstr, 10), $len, '0', STR_PAD_LEFT) . ' ';
				}
				$ret .= PHP_EOL;
			}
		}
		else {
			foreach ($this as $i => $row) {
				foreach ($row as $j => $d) {
					$dstr = sprintf($format, $d);
					$len  = $maxlen - strlen ($dstr);
					$ret .= $dstr[0] . str_repeat('0', $len) . substr($dstr, 1) . ' ';
				}
				$ret .= PHP_EOL;
			}
		}
		
		return $ret;
	}
	
	public function transpose () {
		$ret = array();
		list ($nrow, $ncol) = $this->count();
		for ($j=0; $j<$ncol; $j++) {
			$ret[$j] = array();
			for ($i=0; $i<$nrow; $i++) {
				$ret[$j][$i] = $this->data[$i][$j];
			}
		}
		return new self($ret);
	}
	
	public function t () {
		return $this->transpose();
	}
	
	public function row_splice ($offset, $length = null, $replacement = array()) {
		list ($nrow, $ncol) = $this->count();
		if (is_null($length)) $length = $nrow;
		if (!empty($replacement)) {
			$isarray = array_map('is_array', $replacement);
			if (!array_product($isarray))
				throw new \InvalidArgumentException('Replacement for row_splice must be an 2D-array.');
			
			$sizes  = array_map('sizeof', $replacement);
			if ($sizes != array_fill(0, sizeof($replacement), $ncol))
				throw new \InvalidArgumentException('Mismatched number of columns in replacement array.');
		}
		array_splice ($this->data, $offset, $length, $replacement);
	}
	
	/**
	 * Note that $replacement will be transposed
	 * meaning that [1,2,3] will be insert as column instead of [[1], [2], [3]]
	 */
	public function col_splice ($offset, $length = null, $replacement = array()) {
		list ($nrow, $ncol) = $this->count();
		if (is_null($length)) $length = $ncol;
		if (!empty($replacement)) {
			$isarray = array_map('is_array', $replacement);
			if (!array_product($isarray))
				throw new \InvalidArgumentException('Replacement for col_splice must be an 2D-array.');
			
			$sizes  = array_map('sizeof', $replacement);
			if ($sizes != array_fill(0, sizeof($replacement), $nrow))
				throw new \InvalidArgumentException('For replacement [[...n...], [...n...]], n must be equal to number of rows in Matrix');
		}
		// transpose $replacement
		$r = array();
		for ($i=0; $i<$nrow; $i++) {
			for ($j=0; $j<sizeof($replacement); $j++)
				$r[$i][$j] = $replacement[$j][$i];
		}
		
		foreach ($this->data as $i => $row) {
			array_splice ($this->data[$i], $offset, $length, $r[$i]);
		}
	}
	
	/**
	 * @see http://php.net/manual/en/function.fgetcsv.php
	 */
	public function load_csv ($file, $opts = null) {
		$default_opts = array(
			'skipnline' => 0, // skip first n lines
			'delimiter' => ',',
			'enclosure' => '"',
			'escape' => '\\',
			'length' => 0
		);
		
		if (is_null($opts)) $opts = $default_opts;
		else $opts = array_replace($default_opts, $opts);
		
		$f = fopen ($file, 'r');
		if (!$f)
			throw new \InvalidArgumentException("Cannot open csv file: $file");
		
		$nline = 0;
		$ncol = null;
		while (($data = fgetcsv($f, $opts['length'], $opts['delimiter'], $opts['enclosure'], $opts['escape'])) !== false) {
			if ($nline++ < $opts['skipnline']) continue;

			$this->data[] = $data;
			if (is_null($ncol)) $ncol = sizeof($data);
			if ($ncol != sizeof($data))
				throw new \InvalidArgumentException('Inconsistent number of columns in csv file.');
		}
		fclose($f);
	}
	
	public function arr () {
		return $this->data;
	}
	
	public function __call ($name, $arguments) {
		if ($name == 'array')
			return call_user_method_array('arr', $this, $arguments);
	}
}