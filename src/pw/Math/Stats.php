<?php
namespace pw\Math;

class Stats implements  \ArrayAccess, \Iterator, \Countable {
	
	private $data;
	private $computed;
	
    public function __construct($array) {
		if (empty($array)) {
			throw new \InvalidArgumentException ('Stats must perform on non-empty dataset.');
		}
        $this->data = $array;
		$this->computed = array();
    }
	
	public function clear () {
		$this->computed = array();
	}
	
	public function sum () {
		if (!isset($this->computed['sum']))
			$this->computed['sum'] = array_sum($this->data);
		return $this->computed['sum'];
	}
	
	public function power_sum ($p) {
		$ret = 0;
		foreach ($this->data as $d)
			$ret += pow($d, $p);
		return $ret;
	}
	
	public function product () {
		if (!isset($this->computed['product']))
			$this->computed['product'] = array_product($this->data);
		return $this->computed['product'];
	}
	
	public function min () {
		if (!isset($this->computed['min']))
			$this->computed['min'] = min($this->data);
		return $this->computed['min'];
	}
	
	public function max () {
		if (!isset($this->computed['max']))
			$this->computed['max'] = max($this->data);
		return $this->computed['max'];
	}
	
	public function count () {
		if (!isset($this->computed['count']))
			$this->computed['count'] = count($this->data);
		return $this->computed['count'];
	}
	
	public function range () {
		return $this->max() - $this->min();
	}
	
	public function mean () {
		return $this->arithmetic_mean();
	}
	
	public function arithmetic_mean () {
		if (!isset($this->computed['mean']))
			$this->computed['mean'] = $this->sum() / $this->count();
		return $this->computed['mean'];
	}
	
	public function geometric_mean () {
		if (!isset($this->computed['geometric_mean']))
			$this->computed['geometric_mean'] = pow($this->product(), 1.0/$this->count());
		return $this->computed['geometric_mean'];
	}
	
	public function quadratic_mean () {
		return $this->rms();
	}
	
	public function harmonic_mean () {
		if (!isset($this->computed['harmonic_mean'])) {
			$s1 = 0;
			foreach ($this->data as $d)
				$s1 += 1.0/$d;
			$this->computed['harmonic_mean'] = $this->count() / $s1;
		}
		return $this->computed['harmonic_mean'];
	}
	
	public function rms () {
		if (!isset($this->computed['rms'])) {
			$this->computed['rms'] = sqrt($this->power_sum(2)/$this->count());
		}
		return $this->computed['rms'];
	}
	
	public function root_mean_square () {
		return $this->rms();
	}
	
	public function generalized_mean ($p) {
		if ($p <= 0) {
            throw new \InvalidArgumentException('Generalized mean takes p as non-zero positive real number.');
        }

		return pow($this->power_sum($p)/$this->count(), 1/$p);
	}
	
	public function power_mean ($p) {
		return $this->generalized_mean($p);
	}
	
	public function heronian_mean () {
		if ($this->count() != 2) {
			throw new \RuntimeException('Heronian mean takes only two numbers.');
		}
		
		return ($this->data[0] + $this->data[1] + sqrt($this->product())) / 3.0;
	}
	
	public function lehmer_mean ($p) {
		if (!is_numeric($p)) {
            throw new \InvalidArgumentException('P must be a real number');
        }
		
		$top = 0;
        $bottom = 0;

        foreach ($this->data as $v) {
            $top += pow($v, $p);
            $bottom += pow($v, $p - 1);
        }

        return $top / $bottom;
	}
	
	public function contraharmonic_mean () {
		return $this->lehmer_mean(2);
	}
	
	public function midrange () {
		return ($this->max() + $this->min()) / 2;
	}
	
	public function variance () {
		if (!isset($this->computed['variance'])) {
			$this->computed['variance'] = $this->moment(2);
		}
		return $this->computed['variance'];
	}
	
	public function moment ($k) {
		if (!isset($this->computed['moment'])) {
			$ret = 0;
			foreach ($this->data as $d) {
				$ret += pow ($d-$this->mean(), $k);
			}
			$this->computed['moment'] = $ret/$this->count();
		}
		return $this->computed['moment'];
	}
	
	public function stdev () {
		return sqrt($this->variance());
	}
	
	public function stddev () {
		return $this->stdev();
	}
	
	public function standard_deviation () {
		return $this->stdev();
	}
	
	public function sample_variance () {
		if (!isset($this->computed['sample_variance'])) {
			$ret = 0;
			foreach ($this->data as $d) {
				$ret += pow ($d-$this->mean(), $k);
			}
			$this->computed['sample_variance'] = $ret/($this->count() - 1);
		}
		return $this->computed['sample_variance'];
	}
	
	public function kurtosis () {
		if (!isset($this->computed['kurtosis'])) {
			$this->computed['kurtosis'] = ($this->moment(4) / pow($this->moment(2), 2)) - 3;
		}
		return $this->computed['kurtosis'];
	}
	
	/**
	 * @see https://en.wikipedia.org/wiki/Quartile
	 * @return array  The quartile values
	 */
	public function quartile() {
        $data = $this->arr;
        sort($arr, SORT_NUMERIC);
		
		$ret = array();
		$ret[0] = $data[floor($this->count() / 4) - 1];
		$ret[2] = $data[3 * floor($this->count() / 4) - 1];
		if ($this->count() & 1)
			$ret[1] = $data[floor($this->count() / 2)];
		else
			$ret[1] = ($data[floor($this->count() / 2) - 1] + $data[floor($this->count() / 2)]) / 2;

    }
	
	public function median () {
		if (!isset($this->computed['median'])) {
			list (, $median) = $this->quartile();
			$this->computed['median'] = $median;
		}
		return $this->computed['median'];
	}
	
	public function interquartile_range () {
		list ($r1, , $r2) = $this->quartile();
		return $r2 - $r1;
	}
	
	public function iqr () {
		return $this->interquartile_range();
	}

    public function skewness() {
		if (!isset($this->computed['skewness'])) {
			$this->computed['skewness'] = $this->moment(3) / pow($this->moment(2), 1.5);
		}
		return $this->computed['skewness'];
    }
	
	/**
	 * coefficientOfVariation
	 */
	public function cv () {
		return $this->stdev() / $this->mean();
	}
	
	/**
	 * indexOfDispersion, variance-to-mean ratio
	 */
	public function vmr () {
		return $this->variance() / $this->mean();
	}
	
	/**
	 * Implement get function of array []
	 * @param mixed $offset The index
	 * @return mixed  the value
	 */
    public function offsetGet($offset) {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }
	
	public function offsetSet ($offset, $value) {
		if (is_null($offset) and is_array($value)) {
			$this->data = array_merge($this->data, $value);
		}
		elseif (is_null($offset)) $this->data[] = $value;
		else $this->data[$offset] = $value;
		$this->clear();
	}
	
	public function offsetExists ($offset) {
		return isset($this->data[$offset]);
	}
	
	public function offsetUnset($offset) {
        unset($this->data[$offset]);
    }
	
	function rewind() {
		return reset($this->data);
	}
	function current() {
		return current($this->data);
	}
	function key() {
		return key($this->data);
	}
	function next() {
		return next($this->data);
	}
	function valid() {
		return key($this->data) !== null;
	}
}