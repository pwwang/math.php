<?php

/**
 * Calculate GSEA
 */
namespace pw\Math\Test;
use pw\Math\Rank;

class GSEA {
	
	private $refset;
	private $es;
	private $corrs;
	private $weight;
	
	/**
	 * Construct
	 * Genes have to be pre-ranked, so we can only do gene permutation
	 * instead of phenotype label permutation to get p-value
	 * 
	 * @param Rank|Array $corr The rankings
	 * 	The corr could be a pre-ranked array, e.g. ['a', 'b', 'c'], meaning
	 * 	'a' ranks higher than 'b' and 'c', then the association score
	 * 	will be used as 1.
	 * 	Or an array with scores given, e.g. ['a' => 0.1, 'b' => 0.3, 'c' => 0.2],
	 * 	indicating the 'b' is the topest rank.
	 * @param Array $refset The leading set, with keys of $corr
	 * @param Number $weight The weight, the default value is 1, if $weight = 0,
	 * 	it falls into Kolmogorov-Smirnov test
	 */
	public function __construct ($corr, $refset, $weight = 1) {
		// change $leading to Hash so that computation will be faster
		$this->refset = array();		
		foreach ($refset as $l) $this->refset[$l] = 1;
		
		if (isset($corr[0])) { // a pre-ranked list
			$this->corrs = array_combine($corr, array_fill(0, sizeof($corr), 1));
		} else {
			$this->corrs = $corr;
			arsort($this->corrs, SORT_NUMERIC);
		}
		
		$this->weight = $weight;
		$this->es = $this->compute_es($this->corrs, $this->refset);
	}
	
	private function compute_Nr (&$corrs, &$refset) {
		$nr = 0;
		foreach ($refset as $key => $_) {
			$nr += pow (abs($corrs[$key]), $this->weight);
		}
		return $nr;
	}
	
	private function compute_Nm (&$corrs, &$refset) {
		$nh = 0;
		foreach ($corrs as $key => $_)
			if (isset($refset[$key]))
				$nh ++;
		return sizeof($corrs) - $nh;
	}
	
	private function compute_es (&$corrs, &$refset) {
		$es = 0;
		$hits = $miss = 0;
		$nr = $this->compute_Nr ($corrs, $refset);
		$nm = $this->compute_Nm ($corrs, $refset);
		foreach ($corrs as $key => $s) {
			if (isset($refset[$key])) {
				$hits += pow (abs($corrs[$key]), $this->weight) / $nr;
			} else {
				$miss += 1 / $nm;
			}
			if (abs($hits - $miss) > abs($es)) {
				$es = $hits - $miss;
			}					 
		}
		//\pw\Math\Utils::logger("", array('es' => $es));
		return $es;
	}
	
	private function perm_refset ($nperm = 1000, $seed = false) {
		if ($seed === false) $seed = time();
		$ret = array();
		srand ($seed);
		$refsize  = sizeof($this->refset);
		$totalset = array_keys ($this->corrs);
		for ($i=0; $i<$nperm; $i++) {
			$rand_keys = array_rand ($totalset, $refsize);
			$r = array();
			foreach ($rand_keys as $k)
				$r[$totalset[$k]] = 1;
			$ret[] = $r;
		}
		return $ret;
	}
	
	/**
	 * Calculate pvalue by permutating the refset
	 * @return Float  the pvalue
	 */
	public function pval ($nperm = 1000, $seed = false) {
		if ($seed === false) $seed = time();
		$refsets = $this->perm_refset ($nperm, $seed);
		
		$n = 0;
		foreach ($refsets as $rs) {
			$es = $this->compute_es($this->corrs, $rs);
			if (($this->es > 0 and $es >= $this->es) or
				($this->es < 0 and $es <= $this->es))
				$n ++;
		}
		//\pw\Math\Utils::logger("", array('n' => $n));
		return $n/$nperm;
	}
	
	private function compute_nes ($es, &$corrs, &$permed_rs, &$perm_nes = array()) {
		// calculate nes
		$perm_nes = array('+' => array(), '-' => array());
		foreach ($permed_rs as $rs) {
			$es_perm = $this->compute_es ($this->corrs, $rs);
			if ($es_perm > 0) $perm_nes['+'][] = $es_perm;
			if ($es_perm < 0) $perm_nes['-'][] = $es_perm;
		}
		// normalize
		$perm_nes_mean_plus = $perm_nes_mean_minus = 1;
		if (!empty($perm_nes['+'])) {
			$perm_nes_mean_plus  =  array_sum ($perm_nes['+']) / sizeof($perm_nes['+']);
			foreach ($perm_nes['+'] as $i => $es_perm)
				$perm_nes['+'][$i] = $es_perm / $perm_nes_mean_plus;
		}
		if (!empty($perm_nes['-'])) {
			$perm_nes_mean_minus = -array_sum ($perm_nes['-']) / sizeof($perm_nes['-']);
			foreach ($perm_nes['-'] as $i => $es_perm)
				$perm_nes['-'][$i] = $es_perm / $perm_nes_mean_minus;
		}
		
		
		//\pw\Math\Utils::logger("mean_es +: {es}.", array('es' => $perm_nes_mean_plus));
		//\pw\Math\Utils::logger("mean_es -: {es}.", array('es' => $perm_nes_mean_minus));
		return $es > 0 ? $es / $perm_nes_mean_plus : $es / $perm_nes_mean_minus;
	}
	
	public function qval ($nperm = 1000, $nsample = 100, $seed = false) {
		if ($seed === false) $seed = time();
		$refsets = $this->perm_refset ($nperm, $seed);
		// calculate nes
		$permnes = array();
		$nes = $this->compute_nes($this->es, $this->corrs, $refsets, $permnes);
		$n = 0;
		if ($nes > 0)
			foreach ($permnes['+'] as $pnes)
				if ($pnes >= $nes) $n++;
		if ($nes < 0)
			foreach ($permnes['-'] as $pnes)
				if ($pnes <= $nes) $n++;
		//\pw\Math\Utils::logger("ES: {es}, NES: {nes}.", array('es' => $this->es, 'nes' => $nes));
		//\pw\Math\Utils::logger("Number of better sets: {n}/{total}.", array('n' => $n, 'total' => $nperm));
		
		
		$m = $nsign = 0;
		$totalset = array_keys ($this->corrs);
		for ($i=0; $i<$nsample; $i++) {
			shuffle ($totalset);
			$newcorrs = array_combine ($totalset, array_values($this->corrs));
			$ses  = $this->compute_es($newcorrs, $this->refset);
			//\pw\Math\Utils::logger("$ses", $this->refset);
			$snes = $this->compute_nes ($ses, $newcorrs, $refsets);
			//\pw\Math\Utils::logger("", array('snes' => $snes));
			if ($nes * $snes > 0) $nsign ++;
			if (($nes > 0 and $snes >= $nes) or ($nes < 0 and $snes <= $nes))
				$m ++;
		}
		$m = max(1, $m);
		//\pw\Math\Utils::logger("Number of better samples: {m}/{total}.", array('m' => $m, 'total' => $nsign));
		return $n / $nperm * $nsign / $m;
	}
	
	public function fdr ($nperm = 1000, $nsample = 100, $seed = false) {
		return $this->qval($nperm, $nsample, $seed);
	}
	
	public function ES () {
		return $this->es;
	}
	
}