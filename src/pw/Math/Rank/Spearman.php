<?php
namespace pw\Math\Rank;
use pw\Math\Rank;

class Spearman {
	
	private $rank1;
	private $rank2;
	
	public function __construct ($rank1, $rank2) {
		if (sizeof($rank1)!=sizeof($rank2))
			throw new \InvalidArgumentException("The dimension of ranks should be the same");
		
		if ($rank1 instanceof Rank) {
			if ($rank1->direct() != 'desc')
				$rank1->reverse();
			$this->rank1 = $rank1;
		}
		elseif (is_array($rank1)) {
			$this->rank1 = new Rank($rank1, 'desc', Rank::TIE_AVERAGE);
		}
		
		if ($rank2 instanceof Rank) {
			if ($rank2->direct() != 'desc')
				$rank2->reverse();
			$this->rank2 = $rank2;
		}
		elseif (is_array($rank2)) {
			$this->rank2 = new Rank($rank2, 'desc', Rank::TIE_AVERAGE);
		}
	}
	
	public function corr () {
		$d2sum = 0;
		$size = sizeof($this->rank1);
		foreach ($this->rank1 as $key => $value) {
			$d2sum += ($value - $this->rank2[$key]) * ($value - $this->rank2[$key]);
		}
		return 1-6*$d2sum/$size/($size*$size-1);
	}
	
}
