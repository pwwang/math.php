<?php
namespace pw\Math\Rank;
use pw\Math\Rank;

class BordaMerge {
    /**
     * The ranks
     * @var Rank
     */
    protected $ranks;
    
    /**
     * Load ranks
     * 
     * @param array|Rank $array1 The 1st array
     * @param array|Rank $array2 The 2st array
     */
    public function __construct() {
		
		$ranks = array();
		if (func_num_args() == 1) {
			$ranks = func_get_arg(0);
			if (!is_array($ranks))
				throw new \InvalidArgumentException("There must be more than one Arrays/Ranks to merge.");
		} else {
			$ranks = func_get_args();
		}
		
		$size = sizeof($ranks[0]);
		foreach ($ranks as $rank) {
			
			if (sizeof($rank) != $size)
				throw new \InvalidArgumentException("The dimension of ranks should be the same");
			
			if ($rank instanceof Rank) {
				if ($rank->direct() != 'desc')
					$rank->reverse();
				$this->ranks[] = &$rank;
			}
			elseif (is_array($rank)) {
				$this->ranks[] = new Rank($rank, 'desc');
			}
		}
    }
    
    /**
     * Merge the ranks
     * @param string $method The merging method
     * 
     * @return Rank The merged rank
     */
    public function merge ($weight = null) {
		$num = sizeof ($this->ranks);
		if (is_null($weight)) $weight = array_fill(0, $num, 1);
		
		if (sizeof($weight)!=$num)
			throw new \InvalidArgumentException('Number of weights must be as many as Ranks.');
		
		$size = sizeof ($this->ranks[0]);
		$keys = $this->ranks[0]->keys();
		$merged = array();
		for ($i=0; $i<$size; $i++) {
			$key = $keys[$i];
			$merged[$key] = 0;
			foreach ($this->ranks as $j => $rank) { 
				$merged[$key] += ($size - $rank[$key] + 1) * $weight[$j];
			}
		}
        return new Rank ($merged, 'desc');
    }
}
