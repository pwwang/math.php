<?php
namespace pw\Math\Rank;
use pw\Math\Rank;

class Kendall {
	
	private $rank1;
	private $rank2;
	
	public function __construct ($rank1, $rank2) {
		if (sizeof($rank1)!=sizeof($rank2))
			throw new \InvalidArgumentException("The dimension of ranks should be the same");
		
		if ($rank1 instanceof Rank) {
			if ($rank1->direct() != 'desc')
				$rank1->reverse();
			$this->rank1 = $rank1;
		}
		elseif (is_array($rank1)) {
			$this->rank1 = new Rank($rank1, 'desc', Rank::TIE_KEEP);
		}
		
		if ($rank2 instanceof Rank) {
			if ($rank2->direct() != 'desc')
				$rank2->reverse();
			$this->rank2 = $rank2;
		}
		elseif (is_array($rank2)) {
			$this->rank2 = new Rank($rank2, 'desc', Rank::TIE_KEEP);
		}
	}
	
	public function corr () {
		$d = $c = 0;
		$keys  = $this->rank1->keys();
		$n     = sizeof($keys);
		for ($i=0; $i<$n; $i++) {
			for ($j=$i+1; $j<$n; $j++) {
				$k1 = $keys[$i];
				$k2 = $keys[$j];
				$in = ($this->rank1[$k1] - $this->rank1[$k2]) * ($this->rank2[$k1] - $this->rank2[$k2]);
				if ($in > 0) $c ++;
				elseif ($in < 0) $d ++;
			}
		}
		$n1 = $n2 = 0;
		foreach ($this->rank1->nties() as $ntie1) $n1 += $ntie1 * ($ntie1 - 1) / 2;
		foreach ($this->rank2->nties() as $ntie2) $n2 += $ntie2 * ($ntie2 - 1) / 2;
		$n0 = $n * ($n - 1) / 2;
		
		\pw\Math\Utils::logger("n0: {n0}, n1: {n1}, n2: {n2}, nc: {nc}, nd: {nd}", ['n0'=>$n0, 'n1'=>$n1, 'n2'=>$n2, 'nc'=>$c, 'nd' => $d]);
		return ($c - $d) / sqrt (($n0-$n1) * ($n0-$n2));
	}
}
