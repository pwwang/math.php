<?php
namespace pw\Math;

class Utils {
	
	static private $logger = null;
	static private $levels = array();
	
    const DEBUG = 100;
    const INFO = 200;
    const NOTICE = 250;
    const WARNING = 300;
    const ERROR = 400;
    const CRITICAL = 500;
    const ALERT = 550;
    const EMERGENCY = 600;
	
	static public function logger ($msg, $context = array(), $level = self::INFO) {
		if (class_exists('\Monolog\Logger', true)) {
			if (is_null(self::$logger)) {
				self::$logger = new \Monolog\Logger('pw\Math');
				self::$logger->pushHandler(new \Monolog\Handler\ErrorLogHandler());
				self::$logger->pushProcessor(new \Monolog\Processor\PsrLogMessageProcessor());
			}
			self::$logger->addRecord($level, $msg, $context);
		} else {
			if (is_null(self::$logger)) {
				self::$logger = STDERR;
				$class = new \ReflectionClass(__CLASS__);
				foreach ($class->getConstants() as $key => $val)
					self::$levels[$val] = $key;
			}
			$matches = array();
			$replace = array();
			foreach ($context as $key => $val) {
				$matches[] = '{'. $key .'}';
				$replace[] = $val;
			}
			$msg = explode("\n", str_replace ($matches, $replace, $msg));
			
			$prefix  = date("[Y-m-d H:i:s]");
			$prefix .= " pw\Math.". self::$levels. "[$level] ";
			
			$msgs = "";
			foreach ($msg as $m)
				$msgs .= $prefix . $m . PHP_EOL;
			
			fwrite($self::$logger, $msgs);
		}
	}
	
}