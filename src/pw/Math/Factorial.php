<?php
/**
 * Static class to wrap commonly used math functions
 */
namespace pw\Math;

class Factorial {
	
	protected $n;
	
	public function __construct($n) {
		if ($n < 0 || !is_integer($n)) 
            throw new \InvalidArgumentException('Fractional must have positive integers or 0');

		$this->n = $n;
	}
	
	public function log_ret () {
		$ser = (   1.000000000190015
	               + 76.18009172947146   / ($this->n + 2)
	               - 86.50532032941677   / ($this->n + 3)
	               + 24.01409824083091   / ($this->n + 4)
	               - 1.231739572450155   / ($this->n + 5)
	               + 0.12086509738661e-2 / ($this->n + 6)
	               - 0.5395239384953e-5  / ($this->n + 7) );
		$tmp = $this->n + 6.5;
		return ($this->n + 1.5) * log($tmp) - $tmp + log(2.5066282746310005 * $ser / ($this->n+1));
	}
	
	public function ret () {
		$ret = 1;
		while ($this->n) $ret*=$this->n--;
		return $ret;		
	}
	
	public function prox_ret () {
		return exp ($this->log_ret());
	}
	
}