<?php

/**
 * Calculate Pearson's of two data sets
 */
namespace pw\Math\Corr;
use pw\Math\Stats;

class PearsonsR {
	
	protected $data1;
	protected $data2;
	
	public function __construct ($d1, $d2) {
		if ((!is_array($d1) and !($d1 instanceof Stats)) or (!is_array($d2) and !($d2 instanceof Stats)))
			throw new \InvalidArgumentException('Data must be Array or Stats instances for pearson\'s r calculation.');
		
		if (!($d1 instanceof Stats))
			$d1 = new Stats($d1);
		if (!($d2 instanceof Stats))
			$d2 = new Stats($d2);
			
		if (count($d1)!=count($d2))
			throw new \InvalidArgumentException('Data size must be the same to calculation pearson\'s r.');
			
		$this->data1 = $d1;
		$this->data2 = $d2;
	}
	
	private function covariance () {
		$a = ($this->data1->sum() * $this->data2->sum()) / count($this->data1);
		$sum = 0;
		foreach ($this->data1 as $i => $d) {
			$sum += $d * $this->data2[$i];
		}
		return ($sum - $a) / count ($this->data1);
	}
	
	public function r () {
		return $this->covariance() / ($this->data1->stdev() * $this->data2->stdev());
	}
	
	public function p ($method = 'T', $time = 1000) {
		
	}
}