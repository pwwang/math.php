<?php
require_once __DIR__ . "/../vendor/autoload.php";

class testFactorial extends PHPUnit_Framework_TestCase {
	
	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testConstructException () {
		$a = new \pw\Math\Factorial (-1);
	}
	
	public function testConstruct () {
		$a = new \pw\Math\Factorial (1);
		$this->assertInstanceOf ('\pw\Math\Factorial', $a);
	}
	
	/**
	 * @dataProvider test1data
	 */
	public function test1 ($x, $y) {
		$a = new \pw\Math\Factorial($x);
		$this->assertEquals ($a->ret(), $y);
		
		$a = new \pw\Math\Factorial($x);
		$this->assertEquals (log($y), $a->log_ret());
		
		$a = new \pw\Math\Factorial($x);
		$this->assertLessThan (1, abs($y - $a->prox_ret()));
	}
	
	public function test1data () {
		return [
			[0, 1],
			[1, 1],
			[2, 2],
			[3, 6],
			[4, 24],
			[10, 3628800],
			[11, 39916800]
		];
	}
}