<?php
require_once __DIR__ . "/../vendor/autoload.php";
use pw\Math\Test\GSEA;

class testTestGSEA extends PHPUnit_Framework_TestCase {
	
	/**
	 * @dataProvider test1data
	 */
	public function test1 ($x, $y, $z) {
		$gsea = new GSEA ($x, $y);
		$pval = $gsea->pval();
		$qval = $gsea->fdr(10000,100);
		$es = $gsea->es();
		//$this->assertLessThan (0.05, $pval);
		\pw\Math\Utils::logger("", array('p' => $pval, 'q' => $qval, 'es' => $es));
		//$this->assertLessThan (0.25, $qval);
		//\pw\Math\Utils::logger("", array('q' => $qval));
		//\pw\Math\Utils::logger("", array('es' => $qval));
		//$this->assertEquals ($z, $gsea->ES(), '', 0.001);
	}
	
	public function test1data () {
		return [
			[
			  ['a', 'b', 'c', 'd', 'f', 'g', 'h', 'i', 'j', 'k',
			   'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
			   'v', 'w', 'x', 'y', 'z'],
			  ['a', 'd', 'h', 'f'],
			  0.857
			  // {"p":0.002,"q":0.145,"es":0.85714285714286}
			],
			[
			  ['a', 'b', 'c', 'd', 'f', 'g', 'h', 'i', 'j', 'k',
			   'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
			   'v', 'w', 'x', 'y', 'z'],
			  ['d', 'w', 'x', 'y', 'z'],
			  -0.8
			  // {"p":0,"q":0.0285,"es":-0.8}
			],
			[
			  ['a', 'b', 'c', 'd', 'f', 'g', 'h', 'i', 'j', 'k',
			   'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
			   'v', 'w', 'x', 'y', 'z'],
			  ['f', 'n', 'u', 'z', 'p'],
			  -1
			  // {"p":0.343,"q":0.48454054054054,"es":-0.35}
			],
			[
			  ['a', 'b', 'c', 'd', 'f', 'g', 'h', 'i', 'j', 'k',
			   'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
			   'v', 'w', 'x', 'y', 'z'],
			  ['b', 'd', 'g', 'i', 'k'],
			  -1
			  // {"p":0.004,"q":0.3294,"es":0.75}
			]
		];
	}
}