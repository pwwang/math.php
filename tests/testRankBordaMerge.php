<?php
require_once __DIR__ . "/../vendor/autoload.php";

class testRankBordaMerge extends PHPUnit_Framework_TestCase {
	
	public function testConstruct () {
		$a1 = $a2 = [];
		$a = new \pw\Math\Rank\BordaMerge ($a1, $a2);
		$this->assertInstanceOf ('\pw\Math\Rank\BordaMerge', $a);
	}
	
	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testConstructArgs () {
		$a1 = []; $a2 = [1];
		$a = new \pw\Math\Rank\BordaMerge($a1, $a2);
	}
	
	/**
	 * @dataProvider testTheRankBordaMergeProvider
	 */
	public function testTheRankBordaMerge ($x, $y, $w, $z) {
		$rm = new \pw\Math\Rank\BordaMerge($x, $y);
		foreach ($rm->merge($w) as $key => $val) {
			$this->assertEquals ("$key:$val", "$key:{$z[$key]}");
		}
	}
	
	public function testTheRankBordaMergeProvider () {
		return [
			[
				['A' => 1.1, 'B' => 2.1, 'C' => 2.2, 'D' => 4],
				['A' => 1,   'B' => 2,   'C' => 3,   'D' => 4],
				[1, 1],
				['A' => 4,   'B' => 3,   'C' => 2,   'D' => 1],
			],
			[
				['A' => 5.1, 'B' => 8.2, 'C' => 2.2, 'D' => 4],
				['A' => 1,   'B' => 2.4, 'C' => 3,   'D' => 4],
				[1, 10],
				['A' => 4,   'B' => 3,   'C' => 2,   'D' => 1],
			],
		];
	}
}