<?php
require_once __DIR__ . "/../vendor/autoload.php";

class testStats extends PHPUnit_Framework_TestCase {
	
	public function testConstruct () {
		$a1 = [1,2,3];
		$a = new \pw\Math\Stats ($a1);
		$this->assertInstanceOf ('\pw\Math\Stats', $a);
	}
	
	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testConstructArgs () {
		$a1 = [];
		$a = new \pw\Math\Stats($a1);
	}
	
	/**
	 * @dataProvider testSumProvider
	 */
	public function testSum ($x, $y) {
		$rm = new \pw\Math\Stats($x);
		$this->assertEquals($rm->sum(), $y);
		$this->assertEquals($rm->mean(), $y/count($rm));
	}
	
	public function testSumProvider () {
		return [
			[[1,2,3,4,5], 15],
			[[1.2,8,8.1,9,0.3], 26.6]
		];
	}
	
	/**
	 * @dataProvider testPowerSumProvider
	 */
	public function testPowerSum ($x, $y, $z) {
		$rm = new \pw\Math\Stats($x);
		$this->assertEquals($rm->power_sum($y), $z);
	}
	
	public function testPowerSumProvider () {
		return [
			[[1,2,3,4,5], 1, 15],
			[[1,2,3,4,5], 2, 55],
			[[1,2,3,4,5], 3, 225],
			[[1.2,8,8.1,9,0.3], 1, 26.6],
			[[1.2,8,8.1,9,0.3], 2, 212.14],
			[[1.2,8,8.1,9,0.3], 3, 1774.196],
		];
	}
	
	/**
	 * @dataProvider testMinMaxProvider
	 */
	public function testMinMax ($x, $y, $z) {
		$rm = new \pw\Math\Stats($x);
		$this->assertEquals($rm->min(), $y);
		$this->assertEquals($rm->max(), $z);
		$this->assertEquals($rm->range(), $z-$y);
	}
	
	public function testMinMaxProvider () {
		return [
			[[1,2,3,4,5], 1, 5],
			[[1.2,8,8.1,9,0.3], .3, 9],
		];
	}
	
	/**
	 * @dataProvider testCountProvider
	 */
	public function testCount ($x, $y) {
		$rm = new \pw\Math\Stats($x);
		$this->assertEquals($rm->count(), $y);
		$this->assertEquals(count($rm), $y);
	}
	
	public function testCountProvider () {
		return [
			[[1,2,3,4,5], 5],
			[[1.2,8,8.1,9,0.3,2], 6],
		];
	}
}