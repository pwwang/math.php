<?php
require_once __DIR__ . "/../vendor/autoload.php";

class testMatrix extends PHPUnit_Framework_TestCase {
	
	public function testConstruct () {
		$a1 = [[1,2,3], [4,5,6], [7,8,9]];
		$a = new \pw\Math\Matrix ($a1);
		$this->assertInstanceOf ('\pw\Math\Matrix', $a);
	}
	
	/**
	 * @expectedException \InvalidArgumentException
	 * @dataProvider testExceptionProvider
	 */
	public function testException ($a) {
		new \pw\Math\Matrix($a);
	}
	
	public function testExceptionProvider () {
		return [
			[[
				[1,2],
				[1],
				3
			]],
			[[
				3,
				[1,2]
			]],
			[[
				[1,2],[3]
			]]
		];
	}
	
	public function testFill () {
		$a = new \pw\Math\Matrix();
		$a->fill (0, 3, 3);
		$this->assertEquals((string)$a, (string)(new \pw\Math\Matrix([[0,0,0],[0,0,0],[0,0,0]])));
	}
	
	public function testRandomFill () {
		$a = new \pw\Math\Matrix();
		srand (1);
		$a->rand_fill (3, 3, 0, 1);
		//echo $a;
		$b = new \pw\Math\Matrix();
		srand (1);
		$b->rand_fill (3, 3, 0, 1);
		$this->assertEquals((string)$a, (string)$b);
	}
	
	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testSubscribeException1 () {
		$a1 = [[1,2,3], [4,5,6], [7,8,9]];
		$a = new \pw\Math\Matrix ($a1);
		
		$a["a"];
	}
	
	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testSubscribeException2 () {
		$a1 = [[1,2,3], [4,5,6], [7,8,9]];
		$a = new \pw\Math\Matrix ($a1);
		
		$a[3];
	}
	
	public function testSubscribe () {
		$a1 = [[1,2,3], [4,5,6], [7,8,9]];
		$a = new \pw\Math\Matrix ($a1);
		
		$this->assertEquals ((string) $a[1], (string) new \pw\Math\Matrix([[4,5,6]]));
		$this->assertEquals ((string) $a[',1'], (string) new \pw\Math\Matrix([[2],[5],[8]]));
		$this->assertEquals ((string) $a['1,1'], (string) new \pw\Math\Matrix([[5]]));
		$this->assertEquals ((string) $a['1,1']->get(), '5');
		$this->assertEquals ((string) $a->get(1,1), '5');
		$this->assertEquals ((string) $a[':1,:1'], (string) new \pw\Math\Matrix([[1,2],[4,5]]));
		$this->assertEquals ((string) $a[':1,:1'], (string) $a['0:1,:1']);
		$this->assertEquals ((string) $a['0:1,:'], (string) new \pw\Math\Matrix([[1,2,3],[4,5,6]]));
		
	}
	
	public function testModify () {
		$a1 = [[1,2,3], [4,5,6], [7,8,9]];
		$a = new \pw\Math\Matrix ($a1);
		$a[] = [[10,11,12]];
		
		$b = new \pw\Math\Matrix([[1,2,3], [4,5,6], [7,8,9], [10,11,12]]);
		$this->assertEquals(strval($a), strval($b));
		$b['1:2,:1'] = [[1,1],[1,1]];
		$c = new \pw\Math\Matrix([
			[1,2,3],
			[1,1,6],
			[1,1,9],
			[10,11,12]
		]);
		$this->assertEquals(strval($b), strval($c));
		
		$c['2:5, 2:4'] = [[.1,.2,.3], [.4,.5,.6], [.7,.9,.8], [.10,1.1,.1]];
		$d = new \pw\Math\Matrix([
			[1,2,3,0,0],
			[1,1,6,0,0],
			[1,1,.1,.2,.3],
			[10,11,.4,.5,.6],
			[0,0,.7,.9,.8],
			[0,0,.1,1.1,.1]
		]);
		$this->assertEquals(strval($c), strval($d));
		
		$e = new \pw\Math\Matrix($a1);
		$e['4,4'] = [[100]];
		$f = new \pw\Math\Matrix([
			[1,2,3,0,0],
			[4,5,6,0,0],
			[7,8,9,0,0],
			[0,0,0,0,0],
			[0,0,0,0,100]
		]);
		$this->assertEquals(strval($e), strval($f));
	}
	
	/**
	 * @dataProvider testTransposeProvider
	 */
	public function testTranspose ($a, $b) {
		$m = new \pw\Math\Matrix($a);
		$t = new \pw\Math\Matrix($b);
		$this->assertEquals(strval($m->t()), strval($t));
	}
	
	public function testTransposeProvider() {
		return [
			[
			 [
				[1,2,3]
			 ],
			 [
				[1],[2],[3]
			 ]
			],
			[
			 [
				[1],[2],[3]
			 ],
			 [
				[1,2,3]
			 ]
			],
			[
			 [
				[1,2,3], [4,5,6]
			 ],
			 [
				[1,4],[2,5],[3,6]
			 ]
			]
		];
	}
	
	/**
	 * @dataProvider testRowSpliceProvider
	 */
	public function testRowSplice ($array, $offset, $len, $replacement, $result) {
		$m = new \pw\Math\Matrix ($array);
		$m->row_splice ($offset, $len, $replacement);
		
		$r = new \pw\Math\Matrix ($result);
		
		$this->assertEquals (strval($m), strval($r));
	}
	
	public function testRowSpliceProvider() {
		return [
			[
			 [	[1,2,3]	 ],
			 0, 0,
			 [	[0,1,2]  ],
			 [	[0,1,2], [1,2,3] ]
			],
			[
			 [	[1],[2],[3]	 ],
			 1, null,
			 [  [10] ],
			 [	[1], [10]  ]
			],
			[
			 [	[1,2,3], [4,5,6] ],
			 2, 0,
			 [	[7,8,9] ],
			 [	[1,2,3], [4,5,6], [7,8,9] ]
			]
		];
	}
	
	
	/**
	 * @dataProvider testColSpliceProvider
	 */
	public function testColSplice ($array, $offset, $len, $replacement, $result) {
		$m = new \pw\Math\Matrix ($array);
		$m->col_splice ($offset, $len, $replacement);
		
		$r = new \pw\Math\Matrix ($result);
		
		$this->assertEquals (strval($m), strval($r));
	}
	
	public function testColSpliceProvider() {
		return [
			[
			 [	[1,2,3]	 ],
			 0, 0,
			 [	[0]  ],
			 [	[0,1,2,3] ]
			],
			[
			 [	[1],[2],[3]	 ],
			 1, null,
			 [  [10, 20, 30] ],
			 [	[1, 10], [2, 20], [3, 30]  ]
			],
			[
			 [	[1,2,3], [4,5,6] ],
			 2, 0,
			 [	[7,6],[8,9] ],
			 [	[1,2,7,8,3], [4,5,6,9,6] ]
			]
		];
	}
	
	public function testLoadCsv () {
		$m = new \pw\Math\Matrix();
		$m ->load_csv(__DIR__ . "/testMatrix.csv", [
			'skipnline' => 1			
		]);
		$a1 = [[1,2,3], [3,4,6], [2,9,0]];
		$a = new \pw\Math\Matrix ($a1);
		$this->assertEquals (strval($a), strval($m));
	}
	
	public function testGetArray () {
		$a1 = [[1,2,3], [3,4,6], [2,9,0]];
		$a = new \pw\Math\Matrix ($a1);
		
		$this->assertEquals ($a->array(), $a1);
	}
}