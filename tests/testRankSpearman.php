<?php
require_once __DIR__ . "/../vendor/autoload.php";
use \pw\Math\Rank;
use \pw\Math\Rank\Spearman;

class testRankSpearman extends PHPUnit_Framework_TestCase {
	
	public function testConstruct () {
		$a1 = $a2 = [];
		$a = new Spearman ($a1, $a2);
		$this->assertInstanceOf ('\pw\Math\Rank\Spearman', $a);
	}
	
	/**
	 * @expectedException \InvalidArgumentException
	 */
	public function testConstructArgs () {
		$a1 = []; $a2 = [1];
		$a = new Spearman($a1, $a2);
	}
	
	/**
	 * @dataProvider testTheRankBordaMergeProvider
	 */
	public function testCorr ($x, $y, $z) {
		$rm = new Spearman($x, $y);
		$this->assertEquals ($z, $rm->corr(), '', 0.001);
	}
	
	public function testTheRankBordaMergeProvider () {
		return [
			[
				[106, 86, 100, 101, 99, 103, 97, 113, 112, 110],
				[7, 0, 27, 50, 28, 29, 20, 12, 6, 17],
				-0.175757575
			],
			[
				[106, 86, 100, 101, 99, 103, 97, 113, 112, 110, 0, 0, 0, 0, 0],
				[7, 0, 27, 50, 28, 29, 20, 12, 6, 17, 0, 0, 0, 0, 0],
				-0.175757575
			]
		];
	}
}