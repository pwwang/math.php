<?php
require_once __DIR__ . "/../vendor/autoload.php";

class testRank extends PHPUnit_Framework_TestCase {
	
	public function testConstruct () {
		$a = new \pw\Math\Rank ([]);
		$this->assertInstanceOf ('\pw\Math\Rank', $a);
	}
	
	/**
	 * @dataProvider testTheRankProvider
	 */
	public function testTheRank ($x, $y) {
		$r = new \pw\Math\Rank($x, 'asc');
		foreach ($r as $key => $val) {
			$this->assertEquals ("$key:$val", "$key:{$y[$key]}");
		}
		$this->assertEquals(4, count($r));
	}
	
	public function testTheRankProvider () {
		return [
			[	['A' => 1.1, 'B' => 2.1, 'C' => 2.2, 'D' => 4],
				['A' => 1,   'B' => 2,   'C' => 3,   'D' => 4]   ],
			[	['A' => 1,   'B' => 3,   'C' => 3,   'D' => 4],
				['A' => 1,   'B' => 2,   'C' => 2,   'D' => 4]   ],
			[	['A' => 4.1, 'B' => 3,   'C' => 3.1, 'D' => 1],
				['A' => 4,   'B' => 2,   'C' => 3,   'D' => 1]   ],
			[	['A' => 4.1, 'B' => 3.1, 'C' => 3.1, 'D' => 1],
				['A' => 4,   'B' => 2,   'C' => 2,   'D' => 1]   ],
		];
	}
}